package main

type ErrorResponse struct {
	Error string `json:"error"`
}

type Versions struct {
	Version   string `json:"version"`
	Commit    string `json:"commit"`
	BuildTime string `json:"buildTime"`
}

type Book struct {
	Authors []Author `json:"authors"`
}

type Author struct {
	Key string `json:"key"`
}

type Name struct {
	Name string `json:"personal_name"`
}

type RespAuthor struct {
	Name string `json:"name"`
	Key  string `json:"authorKey"`
}

type Entries struct {
	Entries []Books `json:"entries"`
}
type Books struct {
	Title    string `json:"title"`
	Key      string `json:"key"`
	Revision int    `json:"revision"`
	Created  struct {
		Value string `json:"value"`
	} `json:"created"`
}
type RespBooks struct {
	Name        string `json:"name"`
	Key         string `json:"key"`
	Revision    int    `json:"revision"`
	PublishDate string `json:"publishDate"`
}

// config

type Config struct {
	Server      Server
	OpenLibrary OpenLibrary
	Censors     []string `mapstructure:"censors"`
}

type Server struct {
	Addr       string `mapstructure:"addr"`
	BaseApiUrl string `mapstructure:"baseApiUrl"`
	SwaggerUrl string `mapstructure:"swaggerUrl"`
	MetricsUrl string `mapstructure:"metricsUrl"`
}

type OpenLibrary struct {
	BaseUrl     string `mapstructure:"baseUrl"`
	ResultLimit uint16 `mapstructure:"resultLimit"`
}

/*type Censors struct {
	authorID []string
}*/
