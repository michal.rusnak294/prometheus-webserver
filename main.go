package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	_ "prometheus-webserver/docs"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

var appVersion, commit, buildTime string
var versions Versions
var censors = make(map[string]bool)
var mu sync.Mutex
var config Config

var (
	httpRequestCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "The total number of http requests",
	}, []string{"code", "url"})
)

// @title     Golang Bookstore API
// @version         1.0.0
// @description     A book management service API in Go using Gin framework.
// @contact.name   Michal
// @contact.url    https://twitter.com/
// @host      localhost:8080
// @BasePath  /api/v1
func main() {
	versions.Version = appVersion
	versions.Commit = commit
	versions.BuildTime = buildTime

	// var config Config

	viper.SetConfigName("config") // name of config file (without extension)
	viper.SetConfigType("yaml")   // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(".")      // optionally look for config in the working directory
	err := viper.ReadInConfig()   // Find and read the config file
	if err != nil {               // Handle errors reading the config file
		fmt.Println("fatal error config file: %w", err)
	}

	/*if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
		} else {
			// Config file was found but another error was produced
		}
	}*/

	err = viper.Unmarshal(&config)

	if err != nil {
		fmt.Println("error", err)
	}

	setCensors(config.Censors)
	r := gin.Default()
	r.Use(incCounter)

	r.GET(config.Server.BaseApiUrl+"/v1/version", version) // api url
	r.GET(config.Server.BaseApiUrl+"/v1/authors", getAuthors)
	r.GET(config.Server.BaseApiUrl+"/v1/books", getBooks)
	r.POST(config.Server.BaseApiUrl+"/v1/censors", enpSetCensors)
	r.GET(config.Server.MetricsUrl, gin.WrapH(promhttp.Handler()))

	r.GET(config.Server.SwaggerUrl, ginSwagger.WrapHandler(swaggerFiles.Handler))

	srv := &http.Server{
		Addr:    config.Server.Addr + ":8080", // port
		Handler: r,
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	log.Print("Server Started")

	<-done
	log.Print("Server Stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second) // waits 20 sec before exit
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}

	log.Print("Server Exited Properly")

}

// version             godoc
// @Summary      Get app version
// @Description  Returns the version, commit hash and build time.
// @Tags         version
// @Produce      json
// @Success      200  {array}  Versions
// @Router       /version [get]
func version(c *gin.Context) {
	time.Sleep(1 * time.Second)
	c.IndentedJSON(http.StatusOK, Versions{Version: versions.Version, Commit: versions.Commit, BuildTime: versions.BuildTime})
}

// getAuthors             godoc
// @Summary      Get authors of book
// @Description  Returns the authors of specified book.
// @Tags         book
// @Produce      json
// @Success      200  {array}  RespBooks
// @Failure      400  {object} ErrorResponse
// @Failure      500  {object} ErrorResponse
// @Router       /authors [get]
// @Param        book query string false "ISBN Book"
func getAuthors(c *gin.Context) {
	bookAuthors := Book{}
	name := Name{}

	book := c.Query("book")
	fmt.Println("book:", book)
	if book == "" {
		c.IndentedJSON(http.StatusBadRequest, ErrorResponse{Error: "missing URL param book which has to contain isbn of book"})
		return
	}

	// TODO: ISBN regex check

	url := config.OpenLibrary.BaseUrl + "/isbn/" + book + ".json" // toto

	body, err := apiCall(url)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	err = json.Unmarshal(body, &bookAuthors)
	if err != nil {
		fmt.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	respA := make([]RespAuthor, len(bookAuthors.Authors))

	for index := range bookAuthors.Authors {
		url = config.OpenLibrary.BaseUrl + bookAuthors.Authors[index].Key + ".json"
		body, err = apiCall(url)
		if err != nil {
			c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
			return
		}
		err := json.Unmarshal(body, &name)
		if err != nil {
			fmt.Println("error:", err)
			c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
			return
		}

		bookAuthors.Authors[index].Key = strings.ReplaceAll(bookAuthors.Authors[index].Key, "/authors/", "")

		respA[index].Name = name.Name
		respA[index].Key = bookAuthors.Authors[index].Key
	}

	c.IndentedJSON(http.StatusOK, respA)

}

func getBooks(c *gin.Context) {
	authorBooks := Entries{}

	author := c.Query("author")
	if author == "" {
		c.IndentedJSON(http.StatusBadRequest, ErrorResponse{Error: "missing URL param book which has to contain isbn of book"})
		return
	}
	if isCensored(author) {
		c.IndentedJSON(http.StatusForbidden, ErrorResponse{Error: "this author is censored and his/her books will not be shown"})
		return
	}

	url := config.OpenLibrary.BaseUrl + "/authors/" + author + "/works.json?limit=" + strconv.Itoa(int(config.OpenLibrary.ResultLimit))
	body, err := apiCall(url)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	err = json.Unmarshal(body, &authorBooks)
	if err != nil {
		fmt.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}

	respB := make([]RespBooks, len(authorBooks.Entries))

	for index := range authorBooks.Entries {

		respB[index].Name = authorBooks.Entries[index].Title
		respB[index].Key = authorBooks.Entries[index].Key
		respB[index].Revision = authorBooks.Entries[index].Revision
		respB[index].PublishDate = authorBooks.Entries[index].Created.Value
	}

	c.IndentedJSON(http.StatusOK, respB)
}

func enpSetCensors(c *gin.Context) {

	reqCensors := []string{}

	err := c.BindJSON(&reqCensors)
	if err != nil {
		log.Println(err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "internal error"})
		return
	}
	setCensors(reqCensors)

	censorswrite := make([]string, 0)
	for censor := range censors {
		censorswrite = append(censorswrite, censor)
	}

	viper.Set("censors", censorswrite)
	viper.WriteConfig()

	c.Status(http.StatusOK)

}

func setCensors(cens []string) {
	mu.Lock()
	for _, c := range cens {
		censors[c] = true
	}
	mu.Unlock()
	// fmt.Println(censors)
}

func isCensored(author string) bool {
	mu.Lock()
	defer mu.Unlock()
	return censors[author]
}

func apiCall(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()

	// !!! nacitava cely []byte do pamate, lepsie bufio.NewReader()
	body, err := ioutil.ReadAll(resp.Body)

	return body, err
}

func incCounter(c *gin.Context) {
	url := c.Request.URL.String()
	c.Next()
	code := strconv.Itoa(c.Writer.Status())

	//httpRequestCounter.WithLabelValues(code, url).Inc()
	counter, err := httpRequestCounter.GetMetricWith(prometheus.Labels{"code": code, "url": url})
	if err != nil {
		log.Println(err)
	} else {
		counter.Inc()
	}

}

// merge request try
// curl localhost:8080/api/v1/books?author=OL1394244A
